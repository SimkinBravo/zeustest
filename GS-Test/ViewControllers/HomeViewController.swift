//
//  HomeViewController.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Alamofire

class HomeViewController: CollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        registerCollectionViewCells()
        GraphQLApi.shared.loadUrl(query: "{ __schema { types { name fields { name description } } } }") { response in
            switch(response) {
            case .success(let result):
                print("result: ", result)
                guard let data = result as? [String: AnyObject] else { return }
                print("data: ", data)
                data.forEach { row in
                    print(row)
                }
            case .failure(let error):
                print(error)
            }
        }
    }

    private func registerCollectionViewCells() {

        collectionView.register(NameInputTableViewCell.self, forCellWithReuseIdentifier: NameInputTableViewCell.reuseId)
        collectionView.register(TextButtonCollectionViewCell.self, forCellWithReuseIdentifier: TextButtonCollectionViewCell.reuseId)
        collectionView.register(DescriptionCollectionViewCell.self, forCellWithReuseIdentifier: DescriptionCollectionViewCell.reuseId)
    }

}

extension HomeViewController {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        switch indexPath.row {

        case 1:
            let controller = SelfieController()
            present(controller, animated: true)

        case 2:
            let controller = ChartsViewController()
            present(controller, animated: true)

        default:
            print("default")
        }
    }
}

class ApplicationData {

    static let shared = ApplicationData()

    private init() {}

    //Production
    let serverUrl = "https://in-log-we-trust.herokuapp.com/v1/graphql"
}

class DataError: Error {

    var message = ""

    init (message: String) {
        self.message = message
    }
}

class GraphQLApi  {

    typealias Handler = (Result<Codable>) -> Void

    static var shared = GraphQLApi()

    init() {   }

    func loadUrl(query: String, then handler: @escaping Handler) {

        var request = URLRequest(url: URL(string: ApplicationData.shared.serverUrl)!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.httpBody = stringify(json: [ "query": query ]).data(using: .utf8)
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.setValue("simkinjerkin", forHTTPHeaderField: "x-hasura-admin-secret")

        Alamofire.request(request).response { response in
            guard let data = response.data else {
                handler(.failure(DataError(message: "No data responded")))
                return
            }

            do {
                let responseJSON = try JSONDecoder().decode(Coda.self, from: data)
                handler(.success(responseJSON))
            } catch(let error) {
                handler(.failure(error))
            }

        }
    }

    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }
}
