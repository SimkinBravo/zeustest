//
//  GraphicViewController.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit

class ChartsViewController: UIViewController {

    var chartsView = ChartsContainer()

    override func loadView() {
        super.loadView()

        view.addSubview(chartsView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        loadData()
    }

    private func loadData() {

        Api.shared.loadData() { result in

            switch(result) {
            case .success(let data):
                self.setCharts(data: data)

            case .failure(let error):
                print(error)
            }
        }
    }

    private func setCharts(data: ChartsData) {
        print(data.colors)

        let colors: [UIColor] = data.colors.compactMap { colorStr in

            return self.hexStringToUIColor(hex: colorStr)
        }
        data.questions.forEach { item in

            chartsView.addQuestion(item, colors: colors)
        }
    }

    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
