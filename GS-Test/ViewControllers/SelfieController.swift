//
//  SelfieViewController.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit

class SelfieController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    var confirmView = ConfirmSelfieView()

    override func loadView() {
        super.loadView()

        view.addSubview(confirmView)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        confirmView.selfieButton.addTarget(self, action: #selector(takePhotoButtonEvent), for: .touchUpInside)
        confirmView.acceptButton.addTarget(self, action: #selector(acceptButtonEvent), for: .touchUpInside)
    }

    @objc func takePhotoButtonEvent() {

        presentCamera()
    }

    @objc func acceptButtonEvent() {

        dismiss(animated: true)
    }

    private func presentCamera() {

        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("simulator")
            confirmView.viewState = .photoIsTaken
            return
        }

        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.delegate = self
        present(vc, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }

        // print out the image size as a test
        print(image.size)
    }
}
