//
//  ConfirmSelfieView.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography

class ConfirmSelfieView: UIView {

    enum States {

        case noPhoto
        case photoIsTaken
    }

    let imageView = UIImageView(image: UIImage(named: "selfie-test"))

    public var viewState: States = .noPhoto {
        didSet {
            setState(viewState)
        }
    }
    var selfieButton = UIButton()
    var acceptButton = UIButton()

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        guard superview != nil else { return }

        initView()
    }

    private func initView() {

        backgroundColor = .white
        constrainElements()
        viewState = .noPhoto

        selfieButton.backgroundColor = .lightGray
        selfieButton.layer.cornerRadius = 10

        acceptButton.backgroundColor = .lightGray
        acceptButton.layer.cornerRadius = 10
        acceptButton.setTitle("Aceptar", for: .normal)
    }

    private func setState(_ state: States) {

        switch state {

        case .noPhoto:
            imageView.isHidden = true
            selfieButton.setTitle("Tomar foto", for: .normal)
            acceptButton.isHidden = true

        case .photoIsTaken:
            imageView.isHidden = false
            selfieButton.setTitle("Tomar nuevamente", for: .normal)
            acceptButton.isHidden = false
        }
    }

    private func constrainElements() {

        let imageMargin: CGFloat = 50
        let margin: CGFloat = 80

        addSubview(imageView)
        addSubview(selfieButton)
        addSubview(acceptButton)

        constrain(imageView, selfieButton, acceptButton, self) { image, selfie, accept, container in

            container.edges == container.superview!.edges

            image.left == container.left + imageMargin
            image.right == container.right - imageMargin
            image.top == container.top + imageMargin
            image.height == 200

            selfie.left == image.left
            selfie.right == image.right
            selfie.top == image.bottom + margin
            selfie.height == 100

            accept.left == image.left
            accept.right == image.right
            accept.top == selfie.bottom + margin
            accept.height == 100
        }
    }
}
