//
//  ChartView.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography
import RKPieChart

class ChartsContainer: UIView {

    let constraintGroup = ConstraintGroup()

    private var charts: [ChartView] = []

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        guard superview != nil else { return }
        backgroundColor = .white
    }

    //todo: buscar mejor arq
    public func addQuestion(_ question: Question, colors: [UIColor]) {

        let items = question.chartData.enumerated().compactMap { index, item in
            return self.getPieElement(chartData: item, color: colors[index])
        }

        let container = ChartView(items: items)
        container.titleLabel.text = question.text
        addSubview(container)
        charts.append(container)

        constrainElements()
    }
 
    func getPieElement(chartData: ChartDataItem, color: UIColor) -> RKPieChartItem {
        print(color)

        return RKPieChartItem(ratio: uint(chartData.percentage), color: color, title: chartData.text)
    }

    private func constrainElements() {

        constrain(charts, replace: constraintGroup) { charts in

            let first = charts.first!

            let container = first.superview!
            container.edges == container.superview!.edges

            first.top == container.top + 20
            first.left == container.left + 20
            first.right == container.right - 20

            align(left: charts)
            align(right: charts)
            distribute(by: 10, vertically: charts)
        }
    }

}

class ChartView: UIView {

    init(items: [RKPieChartItem]) {
        self.chartItems = items

        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var chartItems: [RKPieChartItem]!

    let titleLabel = UILabel()
    var chartView: RKPieChartView!

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        guard superview != nil else { return }

        initView()
    }

    private func initView() {

        chartView = RKPieChartView(items: chartItems)
        chartView.arcWidth = 10

        titleLabel.font = .boldSystemFont(ofSize: 22)

        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping

        addSubview(titleLabel)
        addSubview(chartView)

        constrainElements()
    }

    private func constrainElements() {

        let textMargin: CGFloat = 20
        let chartMargin: CGFloat = 40

        constrain(titleLabel, chartView, self) { title, chart, container in

            title.left == container.left + textMargin
            title.right == container.right - textMargin
            title.top == container.top

            chart.left == container.left + chartMargin
            chart.right == container.right - chartMargin
            chart.height == 250
            chart.top == title.bottom + textMargin

            container.bottom == chart.bottom + chartMargin
        }
    }
}
