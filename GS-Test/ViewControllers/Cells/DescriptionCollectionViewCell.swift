//
//  DescriptionCollectionViewCell.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography

class DescriptionCollectionViewCell: UICollectionViewCell {

    static public let reuseId = "DescriptionCVC"

    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()

    let descriptionText = "Una gráfica o representación gráfica es un tipo de representación de datos, generalmente numéricos, mediante recursos visuales (líneas, vectores, superficies o símbolos), para que se manifieste visualmente la relación matemática o correlación estadística que guardan entre sí. También es el nombre de un conjunto de puntos que se plasman en coordenadas cartesianas y sirven para analizar el comportamiento de un proceso o un conjunto de elementos o signos que permiten la interpretación de un fenómeno. La representación gráfica permite establecer valores que no se han obtenido experimentalmente sino mediante la interpolación (lectura entre puntos) y la extrapolación (valores fuera del intervalo experimental)."

    override init(frame: CGRect) {
        super.init(frame: frame)

        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        titleLabel.font = .boldSystemFont(ofSize: 20)
        titleLabel.text = "Graficas"
        descriptionLabel.text = descriptionText
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.numberOfLines = 0

        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)

        let margin: CGFloat = 20

        constrain(titleLabel, descriptionLabel) { title, desc in

            let container = title.superview!

            title.left == container.left + margin
            title.right == container.right - margin
            title.top == container.top + margin

            desc.left == title.left
            desc.right == title.right
            desc.top == title.bottom + margin
        }
    }

}
