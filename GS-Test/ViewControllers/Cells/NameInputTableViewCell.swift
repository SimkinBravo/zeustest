//
//  NameInputTableViewCell.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography

class NameInputTableViewCell: UICollectionViewCell {

    static public let reuseId = "NameInputCVC"

    private let textfield = UITextField()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        textfield.placeholder = "Nombre"
        textfield.delegate = self
        contentView.addSubview(textfield)

        let margin: CGFloat = 20

        constrain(textfield) { txf in

            let container = txf.superview!

            txf.left == container.left + margin
            txf.right == container.right - margin
            txf.top == container.top + margin
            txf.bottom == container.bottom - margin
        }
    }

}

extension NameInputTableViewCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

                do {
                    let regex = try NSRegularExpression(pattern: ".*[^A-Za-z].*", options: [])

                    if regex.firstMatch(in: string, options: [], range: NSMakeRange(0, string.count)) != nil {
                        return false
                    }
                }
                catch {
                    print("ERROR")
                }
            return true
    }
}
