//
//  TextButtonCollectionViewCell.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography

class TextButtonCollectionViewCell: UICollectionViewCell {

    static public let reuseId = "TextButtonCVC"

    private let button = UIButton()

    override init(frame: CGRect) {
        super.init(frame: frame)

        initElements()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("Interface Builder is not supported!")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        fatalError("Interface Builder is not supported!")
    }

    private func initElements() {

        button.layer.cornerRadius = 10
        button.layer.borderWidth = 1
        button.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        button.setTitle("Tomar selfie", for: .normal)
        button.isUserInteractionEnabled = false

        contentView.addSubview(button)

        let margin: CGFloat = 20

        constrain(button) { btn in

            let container = btn.superview!

            btn.left == container.left + margin
            btn.right == container.right - margin
            btn.top == container.top + margin
            btn.bottom == container.bottom - margin
        }
    }

}
