//
//  SchemaType.swift
//  GS-Test
//
//  Created by Simkin on 22/08/20.
//

import UIKit

struct SchemaType: Codable {

    var name
}
