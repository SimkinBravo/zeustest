//
//  TestData.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit

struct ChartsData: Codable {

    let colors: [String]
    let questions: [Question]
}

struct Question: Codable {

    let total: Float
    let text: String
    let chartData: [ChartDataItem]
}

struct ChartDataItem: Codable {

    enum CodingKeys: String, CodingKey {
        case text
        case percentage = "percetnage"
    }

    let text: String
    let percentage: Float
}
