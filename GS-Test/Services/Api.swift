//
//  Api.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Alamofire

class GenericError: Error {

    let message: String = ""
}

class Api {

    typealias Handler = (Result<ChartsData>) -> Void

    static var shared = Api()

    init() {   }

    func loadData(then handler: @escaping Handler) {

        let request = URLRequest(url: URL(string: "https://us-central1-bibliotecadecontenido.cloudfunctions.net/helloWorld")!)

        Alamofire.request(request).response { response in
            guard let data = response.data else {
                print("No data. Response:", response)
                return
            }

            do {
                let responseJSON = try JSONDecoder().decode(ChartsData.self, from: data)
                handler(.success(responseJSON))
            } catch(let error) {
                handler(.failure(error))
            }

        }
    }

    func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }

}
