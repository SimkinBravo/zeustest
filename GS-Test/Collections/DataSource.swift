//
//  DataSource.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit

class DataSource: NSObject {

//    public var onDataChange: ((Action) -> Void)!
//    public var defaultSectionData: SectionData!

    internal var cellReuseIds: [String] = [NameInputTableViewCell.reuseId, TextButtonCollectionViewCell.reuseId, DescriptionCollectionViewCell.reuseId]

    var numberOfSections: Int {
        return 1
    }

    func getNumberOfItems(for section: Int) -> Int {
        return 3
    }
}

extension DataSource: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellReuseIds[indexPath.row], for: indexPath)
//        setupInteractiveReusableView(cell, at: indexPath, type: .cell)
        return cell

    }
}
