//
//  CollectionViewController.swift
//  GS-Test
//
//  Created by Simkin on 12/08/20.
//

import UIKit
import Cartography

class CollectionViewController: UIViewController {

    let dataSource = DataSource()
    var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
    }

    private func configureCollectionView() {

        if collectionView == nil {
            initCollectionView()
        }
        collectionView.dataSource = dataSource
        collectionView.delegate = self
    }

    internal func initCollectionView() {

        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .clear
        view.addSubview(collectionView)
        constrain(collectionView) { collection in
            collection.edges == collection.superview!.edges
        }
    }
}


extension CollectionViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(UIScreen.main.bounds.width, collectionView.bounds.width)
        let width = UIScreen.main.bounds.width - 20
        let height: CGFloat = indexPath.row == 2 ? 300 : 100
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }

}
